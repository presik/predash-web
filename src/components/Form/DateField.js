import React, {useState} from 'react';
import {Modal, Input} from 'semantic-ui-react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';

import FormField from './FormField';
import tools from 'tools/common';

function DateField(props) {
  let _value = tools.fmtDate(props.value, true);
  // let today = tools.fmtDate2Tryton(new Date());
  let [selectedDate, setSelectedDate] = useState(_value || '');
  const [val, onChange] = useState(new Date());
  const [open, setOpen] = useState(false);

  function handleDateChange(value) {
    const fmtValue = tools.fmtDate2Tryton(value);
    setSelectedDate(fmtValue);
    console.log('fijando valor', fmtValue);
    props.onChange(props.name, fmtValue);
    setOpen(false);
  }

  return (
    <FormField {...props}>
      <Input
        id={props.name}
        name={props.name}
        key={props.name}
        icon="calendar alternate"
        disabled={props.readonly}
        value={selectedDate}
        onClick={() => setOpen(true)}
      />
      <Modal
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
        open={open}>
        <Modal.Header>Seleccione una fecha</Modal.Header>
        <Modal.Content>
          <Calendar onChange={handleDateChange} value={val} />
        </Modal.Content>
      </Modal>
    </FormField>
  );
}

export default DateField;
